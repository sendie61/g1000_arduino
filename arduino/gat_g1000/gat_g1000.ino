/*
16 - Speed brake deployed annunciator
17 - Beacon Light is On
22 - Beacon lights switch
23 - Landing lights Switch (all).
24 - Taxi lights Switch
25 - Nav lights Switch.
26 - Strobe lights switch
27 - Spot lights Switch
28 - static air
30 - Speebrake On/Off / Speebrake On/Off
31 - Avionics Power On (Master switch)
32 - Generator 1 On/Off
33 - Battery 1 Switch / Battery 2 Switch
34 - Fuel pumps switch – all engines
35 - Left Pitot Heat switch / Right Pitot Heat switch
36 - Master anti-ice system Switch (All)
37 - Panel Flood light Switch
38 - Pitch trim up (+) / Pitch trim up (+)
39 - Pitch trim down (-) / Pitch trim down (-)
40 - fuel tank
41 - Right brake Off/Full (switch button)
42 - Fuel Tank
43 - Parking brake None/Full (simple switch)
44 - Left brake Off/Full (switch button)

A0 - flaps request 1 / flap request 0.5 / flaps request 0
A1 - Magneto/Starter Key: OFF / Magneto/Starter Key: RIGHT / Magneto/Starter Key: LEFT / Magneto/Starter Key: BOTH / Magneto/Starter Key: START
A2 - Mixture control, engine #1
A3 - Prop control #1, RPM
A4 - Prop control, manual degrees, all
 */
#define NRDIGIN  (22)
#define NRDIGOUT (2)
#define NRANAIN  (5)

const int digital_inputs[NRDIGIN] = {22,23,24,25,26,27,28,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44};
const int digital_outputs[NRDIGOUT] = {16,17};
const int analog_inputs[NRANAIN] = {0,1,2,3,4};

String cmd = "";
bool cmd_valid = false;
int toggle=0;
int counter=0;

/***********************************************************************************************/
void setup() {
  Serial.begin(2000000);

  for (int i=0; i<NRDIGIN;  ++i) pinMode(digital_inputs[i], INPUT_PULLUP);
  for (int i=0; i<NRDIGOUT; ++i) pinMode(digital_outputs[i], OUTPUT);

  while (!Serial) { ; }
}

/***********************************************************************************************/
void loop()
{
  Serial.write("=");
  for (int i=0; i<NRDIGIN; ++i)
  {
    int pin = digital_inputs[i];
    Serial.write(digitalRead(pin) ? "1" : "0");
  }
  for (int i=0; i<NRANAIN; ++i)
  {
    int pin = analog_inputs[i];
    int val = analogRead(pin);
    Serial.write(",");
    Serial.write(String(val,DEC).c_str());
  }
  Serial.write("\n");

  if (cmd_valid)
  {
    if (cmd.length() == 2)
    {
      for (int i=0; i<NRDIGOUT; i++)
      {
        int pin = digital_outputs[i];
        int val = (cmd[i] == '1');
        digitalWrite(pin,val);
      }
    }

    cmd_valid = false;
    cmd = "";
  }
  delay(100);
}

/***********************************************************************************************/
void serialEvent()
{
  if (!cmd_valid)
  {
    while (Serial.available())
    {
      char ch = (char)Serial.read();
      if (ch == '\n')
      {
        cmd_valid = true;
        break;
      }
      cmd += ch;
    }
  }
}
