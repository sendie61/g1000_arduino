#include "runner.h"
#include <windows.h>
#include <string>
#include <stdexcept>
#include <iostream>
#include <vector>
#include <sstream>
#include "uSerial.h"


using namespace std;

bool dig_inputs[NRDIGIN];
int ana_inputs[NRANAIN];

bool running = false;
bool stopped = true;

std::vector<std::string> split( const std::string &s, char delimiter )
{
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream( s );

    while( std::getline( tokenStream, token, delimiter ) )
    {
        tokens.push_back( token );
    }

    return tokens;
}

/******************************************************************************/
DWORD WINAPI  worker( LPVOID lpParam )
{
    try
    {
		running = true;
		stopped = false;
        Serial ser( "\\\\.\\COM3" );

        ser.open();

        while( running )
        {
            string ans = ser.recv();

            if( ans[0] == '=' )
            {
                string base = ans.substr( 1 );

                vector<string> elems = split( base, ',' );

                // digital inputs
                for( int i = 0; i < NRDIGIN; ++i )
                {
                    dig_inputs[i] = ( base[i] == '1' );
                }

                for( int i = 0; i < NRANAIN; ++i )
                {
                    int val = stoi( elems[1 + i] );
                    ana_inputs[i] = val;
                }
            }
        }

        ser.close();
    }
    catch( const std::exception &e )
    {
        running = false;
        cout << "Thread exception: " << e.what() << endl;
    }

	stopped = true;
    return 0;
}

/******************************************************************************/
void start_worker( void )
{
    DWORD threadid = 0;
    HANDLE h;

    h = CreateThread( NULL, 0, worker, 0, 0, &threadid );

    if( h == NULL )
    {
        throw runtime_error( "CreateThread" );
    }
}
