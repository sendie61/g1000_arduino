#ifndef RUNNER_H
#define RUNNER_H

#include <string>

#define NRDIGIN (22)
#define NRDIGOUT (2)
#define NRANAIN (5)

extern std::string SERIAL_PORT;

extern bool dig_inputs[NRDIGIN];
extern bool dig_outputs[NRDIGOUT];
extern int ana_inputs[NRANAIN];

extern bool running;
extern bool stopped;


void start_worker( void );

#endif
