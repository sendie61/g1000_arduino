#ifndef USERIAL_H
#define USERIAL_H

#include <windows.h>
#include <string>

class Serial
{
public:
    Serial( std::string comport );
    virtual ~Serial();

    void open( void );
    void close( void );

    void send( const std::string &cmd );
    std::string recv();

private:
    HANDLE m_hComm;
    char m_comport[256];
    char m_buffer[256];
    int m_index;
};

#endif
